package com.company;


import org.junit.Test;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.*;

public class MammalBeanTest {

    private MammalBean mammalBean1 = new MammalBean(4, "Brown", 1.2);
    private MammalBean mammalBean2 = new MammalBean(4, "Red", .6);
    private MammalBean mammalBean3 = new MammalBean(4, "Red", .6);

    private DogBean dogBean1 = new DogBean( 4, "Brown", 1.2, "mini pincher", "Parker");
    private DogBean dogBean2 = new DogBean( 4, "Red", .6, "Pomeranian", "Kobi");
    private DogBean dogBean3 = new DogBean( 4, "Brown", 2, "Pit Bull", "Chance");

    @Test
    public void testValues() throws Exception {

        assertEquals(4, mammalBean1.getLegCount());
        assertEquals("Brown", mammalBean1.getColor());
        assertEquals(1.2, mammalBean1.getHeight());
        assertEquals("mini pincher", dogBean1.getbreed());
        assertEquals("Parker", dogBean1.getName());

        assertEquals(4, mammalBean2.getLegCount());
        assertEquals("Red", mammalBean2.getColor());
        assertEquals(.6, mammalBean2.getHeight());
        assertEquals("Pomeranian", dogBean2.getbreed());
        assertEquals("Kobi", dogBean2.getName());

        assertEquals(4, mammalBean3.getLegCount());
        assertEquals("Brown", mammalBean3.getColor());
        assertEquals(1.2, mammalBean3.getHeight());
        assertEquals("Pit Bull", dogBean3.getbreed());
        assertEquals("Chance", dogBean3.getName());
    }

    private Set<MammalBean> mammalBeanSet = new HashSet<>();

    @Test
    public void testSet() throws Exception {

        try {
            mammalBeanSet.add(mammalBean1);
            mammalBeanSet.add(mammalBean2);
            mammalBeanSet.add(mammalBean3);
        }
        catch (Exception ex) {
            System.out.println("Unable to add item to set");
        }
        assertTrue(mammalBeanSet.contains(mammalBean1));
        assertTrue(mammalBeanSet.contains(mammalBean2));
        assertTrue(mammalBeanSet.contains(mammalBean3));

        try{
            mammalBeanSet.remove(mammalBean1);
            mammalBeanSet.remove(mammalBean3);
        }
        catch (Exception ex) {
            System.out.println("Unable to remove item from set");
        }
        assertTrue(!mammalBeanSet.contains(mammalBean2));
    }

    Map<String, DogBean> dogBeanMap = new HashMap<>();

    @Test
    public void testMap() {
        try {
            dogBeanMap.put("Parker", dogBean1);
            dogBeanMap.put("Kobi", dogBean2);
            dogBeanMap.put("Chance", dogBean3);
        }
        catch (Exception ex) {
            System.out.println("Unable to add item to set");
        }
        assertTrue(dogBeanMap.containsKey("Parker"));
        assertTrue(dogBeanMap.containsValue(dogBean1));
        assertTrue(dogBeanMap.containsKey("Kobie"));
        assertTrue(dogBeanMap.containsValue(dogBean2));
        assertTrue(dogBeanMap.containsKey("Chance"));
        assertTrue(dogBeanMap.containsValue(dogBean3));
        try {
            dogBeanMap.remove("Parker");
            dogBeanMap.remove("Chance");
        }
        catch (Exception ex) {
            System.out.println("Unable to remove item from map");
        }
        assertTrue(!dogBeanMap.containsValue(dogBean1));
    }
    @Test
    public void displayComplete() {
        System.out.println("Complete");
    }


}