package com.company;

public class Kennel {
    public DogBean[] buildDogs() {

    DogBean parker = new DogBean(4, "Red",
            2, "Mini Pincher", "Parker");
    DogBean kobi = new DogBean(4, "Blonde",
            1, "Pomeranian", "Kobi");
    DogBean miku = new DogBean(4, "Black",
            3, "Lab", "Miku");
    DogBean honey = new DogBean(4, "Grey",
            3, "Pit", "Honey");
    DogBean chi = new DogBean(4, "Black",
            1, "Pug", "Chi");

    DogBean[] dogs = {parker, kobi, miku, honey, chi};

    return dogs;
    }
    public void displayDogs(DogBean dogs[]) {
        for (DogBean dog: dogs) {

            String dogString = dog.getName() + "\n"
                    + dog.getbreed() + "\n"
                    + Integer.toString(dog.getLegCount()) + " legs" + "\n"
                    + dog.getColor() + "\n"
                    + Double.toString(dog.getHeight()) + " feet tall" + "\n";

            System.out.println(dogString);
        }
    }
}
