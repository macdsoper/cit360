package com.company;

public class Main {
    public static void main(String[] args) {
        Kennel kennel = new Kennel();
        DogBean[] allDogs = kennel.buildDogs();

        kennel.displayDogs(allDogs);
    }
}
