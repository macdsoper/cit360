package com.company;

public class DogBean extends MammalBean {

    private String breed;
    private String name;

    public DogBean(int legCount, String color, double height, String breed, String name) {

        super(legCount, color, height);
        this.breed = breed;
        this.name = name;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public String getbreed() {
        return this.breed;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public String toString() {

        String dogDescription = "Dog's Name - " + getName() + "\n"
                + "Dog's Breed - " + getbreed() + "\n"
                + "Dog's Leg Count - " + getLegCount() + "\n"
                + "color - " + getColor() + "\n"
                + "height - " + getHeight() + "\n";

        return dogDescription;
    }
}
